<?php
declare(strict_types=1);

namespace Plurl;


final class Plurl
{
    private ?string $base;

    public function __construct(?string $base)
    {
        $this->base = $base;
        if ($base) {
            $this->base = $this->addTrailingSlash($base);
        }
    }

    private function addTrailingSlash(string $url): string
    {
         if (substr($url, -1) !== '/') {
             return $url . '/';
         }
         return $url;
    }

    private function removeLeadingSlash(string $path): string
    {
        return ltrim($path, '/');
    }

    public function toAbsolute(?string $path): ?string
    {
        if (!$this->base) {
            return null;
        }
        if (!$path) {
            return $this->base;
        }
        return $this->base . $this->removeLeadingSlash($path);
    }
}