<?php

use Plurl\Plurl;

const EXPECTED = 'http://flibusta.is/b/111317/';

test('toAbsolute 1', function (string $base, string $path) {
    $plurl = new Plurl($base);
    expect($plurl->toAbsolute($path))->toEqual(EXPECTED);
})->with([
    ['http://flibusta.is', '/b/111317/'],
    ['http://flibusta.is/', '/b/111317/'],
    ['http://flibusta.is/', 'b/111317/'],
]);

test('toAbsolute 2', function (?string $expected, ?string $base, ?string $path) {
    $plurl = new Plurl($base);
    expect($plurl->toAbsolute($path))->toEqual($expected);
})->with([
    ['http://flibusta.is/', 'http://flibusta.is', null],
    ['http://flibusta.is/', 'http://flibusta.is/', null],
    [null, null, '/b/111317'],
    [null, null, 'b/111317/'],
    [null, null, null],
]);